(function(){
  var app = angular.module('carsStore-products',[]);
  
  app.directive("productDescription", function() {
    return {
      restrict: 'E',
      templateUrl: "pages/product-description.html",
    };
  });

  app.directive("productSpecs", function() {
    return {
      restrict: 'E',
      templateUrl: "pages/product-specs.html",
    };
  });

  app.directive("productReview", function() {
    return {
      restrict: 'E',
      templateUrl: "pages/product-review.html",
    };
  });

  app.directive("formReview", function() {
    return {
      restrict: 'E',
      templateUrl: "pages/form-review.html",
    };
  });
  // controller in directive(delete ng-controller from index,on tabs)
  app.directive("productTabs", function() {
    return {
      restrict: 'E',
      templateUrl: "pages/product-tabs.html",
      controller: function() {
        this.tab = 1;
    
        this.setTab = function(newValue) {
          this.tab = newValue;
        };
    
        this.isSet = function(tabName) {
          return this.tab === tabName;
        };
    },
    controllerAs: 'tab'
  };
});

  app.directive("productGallery", function() {
    return {
      restrict: 'E',
      templateUrl: "pages/product-gallery.html",
      controller: function() {
        this.current = 0;
        this.setCurrent = function(newGallery) {
          this.current = newGallery || 0;
        };
      },
      controllerAs: 'gallery'
    };
  });
})();