(function() {
  var app = angular.module("carsStore", ['carsStore-products']);

  app.controller("StoreController", function() {
    this.products = cars;
  });

  app.controller('ReviewController',function(){
    this.review = {};
    this.addReview = function(product){
      this.review.createdOn = Date.now();
      product.reviews.push(this.review);
      this.review={};
    };
  });

  app.controller('StoreController', ['$http',function($http){
    var store = this;
    store.products = [];

		$http.get('js/store-products.json').then(function(result){
      store.products = result.data;
    });
  }]);
})();
